# GitDaBooty!

Docker compose with a full suite for sailing the high seas on a vpn with a reverse proxy.

Included in this docker compose:

|   Program    |                 Brief Description                 |           Application Page           |                  Docker Image Source                   |
| :----------: | :-----------------------------------------------: | :----------------------------------: | :----------------------------------------------------: |
|    Bazarr    | Subtitle management companion for Sonarr & Radarr |      https://www.bazarr.media/       |    https://docs.linuxserver.io/images/docker-bazarr    |
|   Jellyfin   |     Open source PLEX alternative media system     |      https://www.jellyfin.org/       |   https://docs.linuxserver.io/images/docker-jellyfin   |
|    Lidarr    |                 Music management                  |  https://wiki.servarr.com/en/lidarr  |    https://docs.linuxserver.io/images/docker-lidarr    |
|   Prowlarr   |                Indexer management                 | https://wiki.servarr.com/en/prowlarr |    https://github.com/linuxserver/docker-prowlarr/     |
|    Radarr    |                  Movie mangement                  |  https://wiki.servarr.com/en/radarr  |    https://docs.linuxserver.io/images/docker-radarr    |
|    Sonarr    |                   TV management                   |  https://wiki.servarr.com/en/sonarr  |    https://docs.linuxserver.io/images/docker-sonarr    |
|   Traefik    |            Docker based reverse proxy             |     https://traefik.io/traefik/      |            https://hub.docker.com/_/traefik            |
| Transmission |          CLI/RPC/WebUi Bittorrent Client          |     https://transmissionbt.com/      | https://docs.linuxserver.io/images/docker-transmission |
|  Wireguard   |            Fast and secure VPN client             |      https://www.wireguard.com/      |  https://docs.linuxserver.io/images/docker-wireguard   |

## Quick setup

### Install docker & docker-compose

e.g. on arch: `pacman -S docker docker-compose --noconfirm`

### IPv6

_IPv6 is disabled by default as it's presently disabled on the box that I host these on._

If you wish to enable ipv6 for your wireguard provider, change `WireguardDisableIPv6=1` to `WireguardDisableIPv6=0` in the `.env` file.

_If you do not enable IPv6, ensure your wireguard config has all IPv6 addresses removed._

### Move your wireguard config

Name your wireguard config `wg0.conf` and place it in `./config/wireguard/`

e.g.: `cp myWireguard.conf ./config/wireguard/wg0.conf`

### Edit the `.env` file

The `.env` file is used to setup your host system as well as what docker compose uses for the containers.

_*#required*_

The lines with a comment, `#required` need to be changed for traefik to set the URL for each service.

Ensure that `Hostname=EnterHostnameHere` and `Domain=EnterDomainHere` are set to the hostname of the host you're running this on and the domain of your home network.

Refer to the comments in `.env` for further details.

### Run `treasurechest-setup.sh`

As `root`, run host setup script `treasurechest-setup.sh`.

There is no error checking in this script and it's very simple. If there are errors you might need to delete the user, group, and/or the paths created.

This script sources the `.env` file and uses the details from that to setup your host system.

It will:

- Create a group named after `groupName` from the `.env` file with a GID of the `PGID` from `.env`

- Create a system user with no shell named after `serviceAccountName` from the `.env` file with a UID of `PUID` from `.env` and with a supplementary group of `groupName` from `.env`.

- Create the paths for the values set in `Downloads`, `Movies`, `TV`, and `Music` folders specified in the `.env` file.

- Create a small hidden file `.connected` in each one of those folders to force sonarr/radarr/lidarr to reobtain files if you have them setup to do so and have experienced signifcant data loss and redeploying to an empty folder.

- Change ownership and set permissions of `Downloads`, `Movies`, `TV`, and `Music` to `nobody:groupName` so everyone in `groupName` can access and delete all files in those directories.

- Change ownership of the `Configs` directory defined in the `.env` file to `serviceAccountName:serviceAccountName` as defined in the `.env` file.

Now that you know what it does, you can run:
`./treasurechest-setup.sh`

### Run docker compose

`docker compose up -d`

### Configure services

If all goes well, all containers and services should now be online

I would recommend setting up transmission followed by prowlarr first and then following the quick start guide on each services wiki as listed in the table above.

Use https://www.ipleak.net and the magnet link they provide to verify your torrent client is using your VPN.

The reverse proxy dashboard is enabled and you can access it at `traefik.YourHostname.Domain` with credentials:

User: `pirate`

PW: `yarrmatey`

All your hosts should should be accesible at the addresses listed below with `YourHostname` and `Domain` replaced with the values from your `.env` file.

`bazarr.YourHostname.Domain`

`jellyfin.YourHostname.Domain`

`lidarr.YourHostname.Domain`

`prowlarr.YourHostname.Domain`

`radar.YourHostname.Domain`

`sonarr.YourHostname.Domain`

`traefik.YourHostname.Domain`

`transmission.YourHostname.Domain`

See you on the high seas!

## Warranty

    Fuck off.
