#!/bin/bash
source .env


echo "Creating group $groupName with GID of $PGID"
groupadd -g $PGID $groupName
echo "Creating user $serviceAccountName with uid of $PUID and no login shell"
useradd -r -M -G $groupName $serviceAccountName -u $PUID -s /bin/false

echo "Creating "$Downloads" "$Movies" "$TV" "$Music" folders"
mkdir -p "$Downloads" "$Movies" "$TV" "$Music"

echo "Adding a small hidden file ($Movies/.connected, $TV/.connected, and $Music/.connected) so sonarr/radarr/lidarr always know the drive is connected and allow it to rebuild the library if there has been a failure."
touch "$Movies"/.connected "$TV"/.connected "$Music"/.connected

echo "Setting ownership of $Downloads, $Movies, $TV, and $Music to nobody:$groupName"
chown -R nobody:$PGID "$Downloads" "$Movies" "$TV" "$Music"

echo "Adjusting permissions so anyone in $groupName can read/write any file in $Downloads, $Movies, $TV, and $Music"
setfacl -R --set u::rwx,g::rwx,o::rx,d:g:$PGID:rwx "$Downloads" "$Movies" "$TV" "$Music"
chmod -R g+s "$Downloads" "$Movies" "$TV" "$Music"

echo "Setting file and directory permissions for any existing files/directories"
find "$Downloads" "$Movies" "$TV" "$Music" -type f -exec chmod u=rw,g=rw,o=r {} \;
find "$Downloads" "$Movies" "$TV" "$Music" -type d -exec chmod u=rwx,g=rwx,o=rx {} \;

echo "Changing ownership of $Configs $serviceAccountName:$serviceAccountName"
chown -R $PUID:$serviceAccountName "$Configs"

echo "Finally, creating external network traefik in docker so we know the IPs and we can reverse proxy other containers outside of this compose"
docker network create   --driver=bridge   --subnet=172.28.0.0/16   --ip-range=172.28.5.0/24 traefik

echo "Basic infrastructure should be setup"
echo "Run:"
echo "docker compose up -d"
